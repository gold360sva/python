# Skeelo Recrutamento

---
![JPG](https://pbs.twimg.com/profile_images/1189989686078562306/HqtcTyhQ_400x400.jpg)


## Bem-vindo

Obrigado por participar do desafio do Skeelo! Estamos muito contentes pelo seu primeiro passo para fazer parte de um time excepcional.

Você deverá criar uma aplicação que irá extrair, transformar e exibir dados da plataforma Reclame Aqui, conforme abaixo:

Instruções:

1. Extrair dados de assunto da reclamação, localização, data e hora e status da reclamação dos registros das páginas de 1 a 10 com o termo "HBO" na página da Vivo no Reclame Aqui. Esse link leva direto para a página 1: --> https://www.reclameaqui.com.br/empresa/vivo-celular-fixo-internet-tv/lista-reclamacoes/?busca=HBO&pagina=1;

![PNG](images/fields_to_scrape.png)

2. Tratar os dados extraídos, principalmente com transformação do campo de data e hora para formato datetime;

3. Salvar os dados tratados em CSV;

4. Exibir gráfico com total de reclamações por dia.


### Linguagem

- Python (ou outra linguagem que produza o mesmo resultado).


### Bibliotecas (sinta-se a vontade quanto a isso, abaixo estão as nossas sugestões):

- Selenium (automação / RPA / web-scraping);
- Pandas (tratamento / transformação de dados);
- Matplotlib / Plotly (visualização de dados).


### Essencial:

- Extrator de dados do Reclame Aqui;
- Tratamento dos dados extraídos;
- Visualização de dados.


### Ganha mais pontos se tiver:

- Prevenção e tratamento de erros.


### Iremos ficar encantados:

- Análise NLP das reclamações extraídas;
- Algo inesperado e surpreendente.


## Processo de entrega:

Após finalizar a implementação do desafio, abra um pull request para este repositório seguindo os passos abaixo:

1. Faça um fork deste repositório, não clone este repositório diretamente;
2. Os commit's e as alterações devem ser feitas no **SEU** fork;
3. Envie um Pull Request;
4. Deixe o fork público para facilitar a inspeção do código.

### ATENÇÃO

Não faça push diretamente para este repositório!